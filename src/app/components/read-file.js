angular
    .module('app')
    .component('readFile', {
        templateUrl: 'app/components/read-file.html',
        controller: function($scope, File) {

            $scope.data = {
                readFileFrom: null,
                format: null,
                readUrl: ""
            };

            var fields = {
                format: {
                    prop: [{
                        name: "codigo"
                    }, {
                        name: "nombre"
                    }],
                    delimiter: /[0-9]+|[A-Za-z ]+/g,
                    key: "nombre"
                },
                newLineFormat: '\n',
                fieldsDelimiter: '/',
                fieldsName: ["provincias", "departamentos", "localidades"]
            };

            $scope.getFile = function() {
                File.getFromUrl($scope.data.readUrl).then(function(textData) {

                    $scope.data.format = File.format(textData, fields);
                    $scope.data.readFileFrom = null;

                }, function() {
                    console.log("get file error");
                });
            };

            $scope.readSuccess = function(textData) {

                $scope.$apply(function() {
                    $scope.data.format = File.format(textData, fields);
                    $scope.data.readFileFrom = null;
                })

            };
        }
    });
