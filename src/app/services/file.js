angular
    .module('app')
    .service('File', function($http, $q) {

        var dataWrapper = {};

        function getFromUrl(urlFile) {
            var deferred = $q.defer();
            // use q de podria haber devuelto directamente $http 
            $http({
                url: urlFile,
                method: 'GET',
            }).then(function(textFile) {
                return deferred.resolve(textFile.data);
            }, function() {
                console.log("get file error");
                return deferred.reject(false);
            });
            return deferred.promise;
        }


        function splitRowFields(row, delimiter) {
            return row.split(delimiter);
        }

        function splitRows(textData, newLineFormat) {
            return textData.split(newLineFormat);
        }

        function group(subFields) {
            var fieldObj = {};
            for (var j = 0; j < subFields.length; j++) {
                if (angular.isDefined(fields.format.prop[j]) && subFields[j] !== "") {
                    fieldObj[fields.format.prop[j].name] = subFields[j];
                }
            }
            return fieldObj;
        }

        function loadSequence(rowData) {

            var insertInTo = [dataWrapper];
            var key = fields.format.key;

            for (var i = 0; i < rowData.length; i++) {

                if (typeof insertInTo[i][fields.fieldsName[i]] == 'undefined') {
                    insertInTo[i][fields.fieldsName[i]] = {};
                }

                if (typeof insertInTo[i][fields.fieldsName[i]][rowData[i][key]] == 'undefined') {
                    insertInTo[i][fields.fieldsName[i]][rowData[i][key]] = rowData[i];
                }

                insertInTo.push(insertInTo[i][fields.fieldsName[i]][rowData[i][key]]);

            }

        }

        function format(textData, fieldsFormat) {
            fields = fieldsFormat;
            dataWrapper = {};
            var rows = splitRows(textData, fields.newLineFormat);
            var i = 0;
            var j = 0;
            for (i; i < rows.length; i++) {
                var rowFields = splitRowFields(rows[i], fields.fieldsDelimiter);
                var rowFormat = [];
                for (j = 0; j < rowFields.length; j++) {
                    var field = rowFields[j].replace(/[^A-Za-z0-9]/g, ' ').trim();
                    if (field !== "") {
                        var subFields = field.match(fields.format.delimiter);
                        var formatField = group(subFields, j);
                        rowFormat.push(formatField);
                    }
                }
                loadSequence(rowFormat);
            }

            return dataWrapper;
        }


        return {
            getFromUrl: getFromUrl,
            format: format
        };
    });
