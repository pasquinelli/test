angular
    .module('app')
    .directive("nestedSelects", function(File, $window) {
        return {
            restrict: 'EA',
            scope: {
                fields: "=",
                dataobj: "="
            },
            templateUrl: 'app/directives/nested.html',
            link: function($scope) {

                $scope.$watch('dataobj', function(newValue, oldValue) {
                    if (newValue != null) {
                        $scope.selectedData = {};
                        $scope.selectedDataFrom = {};
                        for (var i = 0; i < $scope.fields.lenght; i++) {
                            $scope.selectedData[$scope.fields[i]] = {};
                            $scope.selectedDataFrom[$scope.fields[i]] = {};
                        }
                        $scope.nFields = $scope.fields;
                        $scope.selectedDataFrom[$scope.fields[0]] = newValue[$scope.fields[0]];
                    }
                }, true);

                $scope.changeSelects = function(index) {
                    if (index < $scope.fields.length - 1) {
                        var futureField = $scope.fields[index + 1];
                        var field = $scope.fields[index];
                        var selectedValue = $scope.selectedData[field];
                        if (selectedValue !== null) {
                            $scope.selectedDataFrom[futureField] = $scope.selectedDataFrom[field][selectedValue][futureField];
                        }

                    }
                }

            }
        }
    });
