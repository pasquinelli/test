angular
    .module('app')
    .directive("fileRead", function(File, $window) {
        return {
            restrict: 'EA',
            scope: {
                readSuccess: "&"
            },
            template: '<input type="file" />',
            link: function(scope, element, attributes) {

                var fileReader = new $window.FileReader();

                fileReader.onload = function(eventObj) {

                    scope.readSuccess({ textData: eventObj.target.result });

                };

                element.bind("change", function(changeEvent) {

                    fileReader.readAsText(changeEvent.target.files[0]);

                });
            }
        }
    });
